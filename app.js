//Imports
var express = require('express');
var request = require('request');
var cors = require('cors');
var bodyParser = require('body-parser')
var sortJsonArray = require('sort-json-array')

//Initialize express.
var app = express()

//Localhost calls to not work without Access-Control-Allow-Origin header. This library provides this.
app.use(cors())
//Body parser function allows reading of body information from postSortedJokes POST request.
app.use(bodyParser.json());

/*Gather Jokes API
Parameters: None
Returns: JsonDict when successful.
*/
app.get('/getJokes', function(req, res){
    //Notifying when service was called.
    console.log('Getting Jokes!');
    //Used request to connect to the external API source, as it seemed to be a popular function used for this purpose.
    request.get('https://08ad1pao69.execute-api.us-east-1.amazonaws.com/dev/random_ten',
        function (error, response, body) {
            //Verify the call was completed correctly without error before sending back information to application.
            if (!error && response.statusCode === 200) {
                res.setHeader('Content-Type','application/json');
                var jsonObject = JSON.parse(body);

                //If more then 15 results are found, return the first 15.
                if (jsonObject.length > 15){
                    jsonObject = jsonObject.slice(0,15);
                }

                //Send response sorted by id Ascending.
                res.send(sortJsonArray(jsonObject,'id','asc'))
                return;
            }
            //Errors were found display the error code.
            else {
                console.log("There was an error: ") + response.statusCode;
                return;
            }
        });
})

/*Sort API
Parameters:
- jokeJsonDict: Json dictionary of jokes. Required.
- sortField: One of the fields in the joke dictionary to sort by. Default: id.
- sortDirection: Direction to sort options include asc and des. Default: asc.
Returns: Sorted json joke dict.
*/
app.post('/postSortedJokes',function(req, res){
    //Gather parameter values, provided default values for Field and Direction.
    var jokeJsonDict = req.body.jokeJsonDict;
    var sortField = req.body.sortField != null ? req.body.sortField : 'id';
    var sortDirection = req.body.sortDirection != null ? req.body.sortDirection.toLowerCase() : 'asc';
    //Set Content Type.
    res.setHeader('Content-Type','application/json');

    //Sort jokeJsonDict.
    //Used a existing library for cleaner presentation and to save time, updated library to handle number sorting.
    res.send(sortJsonArray(jokeJsonDict, sortField, sortDirection))
})

//Set the Service to listen on port 8000.
app.listen(8000, function () {
    console.log('Joke service listening on port 8000.')
})